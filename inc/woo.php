<?php
/**
 * WooCommerce Template Functions.
 *
 * @package Ecom
 */

add_theme_support('woocommerce');

require get_template_directory() . '/inc/product/account-link.php';
require get_template_directory() . '/inc/product/archive.php';
require get_template_directory() . '/inc/product/cart-link.php';
require get_template_directory() . '/inc/product/checkout-coupon.php';
require get_template_directory() . '/inc/product/custom-fields.php';
require get_template_directory() . '/inc/product/custom-loop-variation-price.php';
require get_template_directory() . '/inc/product/custom-product-loop.php';
require get_template_directory() . '/inc/product/custom-single-product.php';
require get_template_directory() . '/inc/product/percent.php';
require get_template_directory() . '/inc/product/recent-viewed-products.php';
require get_template_directory() . '/inc/product/search-match.php';
require get_template_directory() . '/inc/product/update-cart-count.php';
require get_template_directory() . '/inc/product/update-variable-price.php';


if ( ! function_exists( 'mytheme_woo_cart_available' ) ) {
	function mytheme_woo_cart_available() {
		$woo = WC();
		return $woo instanceof \WooCommerce && $woo->cart instanceof \WC_Cart;
	}
}

if ( ! function_exists( 'mytheme_is_woocommerce_activated' ) ) {
	function mytheme_is_woocommerce_activated() {
		return class_exists( 'WooCommerce' ) ? true : false;
	}
}


// REMOVE SHOP BREADCRUMB

add_filter('woocommerce_get_breadcrumb', 'remove_shop_crumb', 20, 2);
function remove_shop_crumb($crumbs, $breadcrumb)
{
    $new_crumbs = array();
    foreach ($crumbs as $key => $crumb) {
        if ($crumb[0] !== __('Shop', 'Woocommerce')) {
            $new_crumbs[] = $crumb;
        }
    }
    return $new_crumbs;
}

add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args', 20 );
    function jk_related_products_args( $args ) {
        $args['posts_per_page'] = 10; // 4 related products
        //$args['columns'] = 2; // arranged in 2 columns
    return $args;
}