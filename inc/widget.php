<?php 
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function mytheme_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Shop', 'mytheme' ),
		'id'            => 'shop',
		'description'   => esc_html__( 'Add widgets here.', 'mytheme' ),
		'before_widget' => '<div id="%1$s" class="widget shop-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title shop-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 1', 'mytheme' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'mytheme' ),
		'before_widget' => '<div id="%1$s" class="widget footer-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title footer-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 2', 'mytheme' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add widgets here.', 'mytheme' ),
		'before_widget' => '<div id="%1$s" class="widget footer-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title footer-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 3', 'mytheme' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Add widgets here.', 'mytheme' ),
		'before_widget' => '<div id="%1$s" class="widget footer-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title footer-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 4', 'mytheme' ),
		'id'            => 'footer-4',
		'description'   => esc_html__( 'Add widgets here.', 'mytheme' ),
		'before_widget' => '<div id="%1$s" class="widget footer-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title footer-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer bottom', 'mytheme' ),
		'id'            => 'footer-bottom',
		'description'   => esc_html__( 'Add widgets here.', 'mytheme' ),
		'before_widget' => '<div id="%1$s" class="widget footer-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title footer-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Mobile Widgets', 'mytheme' ),
		'id'            => 'mobile-widgets',
		'description'   => esc_html__( 'Add widgets here.', 'mytheme' ),
		'before_widget' => '<div id="%1$s" class="widget mobile-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Page Widgets', 'mytheme' ),
		'id'            => 'page-widgets',
		'description'   => esc_html__( 'Add widgets here.', 'mytheme' ),
		'before_widget' => '<div id="%1$s" class="widget page-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Category Widgets', 'mytheme' ),
		'id'            => 'cat-widgets',
		'description'   => esc_html__( 'Add widgets here.', 'mytheme' ),
		'before_widget' => '<div id="%1$s" class="widget cat-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'mytheme_widgets_init' );