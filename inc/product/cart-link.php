<?php
    if ( ! function_exists( 'mytheme_header_cart' ) ) {
        function mytheme_header_cart() {
            if ( mytheme_is_woocommerce_activated() ) {
                if ( is_cart() ) {
                    $class = 'current-menu-item';
                } else {
                    $class = '';
                }
?>
            <div id="site-header-cart" class="site-header-cart menu">
                <?php mytheme_cart_link(); ?>
                <div class="minicart">
                    <?php the_widget( 'WC_Widget_Cart', 'title=' ); ?>
                </div>
            </div>
    <?php
            }
        }
    }
    
    if ( ! function_exists( 'mytheme_cart_link' ) ) {
        function mytheme_cart_link() {
            if ( ! mytheme_woo_cart_available() ) {
                return;
            }
        ?>
        <a class="cart-contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>"
            title="<?php esc_attr_e( 'View your shopping cart', 'mytheme' ); ?>">
            <div class="icon">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-6 h-6">
                    <path fill-rule="evenodd"
                        d="M7.5 6v.75H5.513c-.96 0-1.764.724-1.865 1.679l-1.263 12A1.875 1.875 0 004.25 22.5h15.5a1.875 1.875 0 001.865-2.071l-1.263-12a1.875 1.875 0 00-1.865-1.679H16.5V6a4.5 4.5 0 10-9 0zM12 3a3 3 0 00-3 3v.75h6V6a3 3 0 00-3-3zm-3 8.25a3 3 0 106 0v-.75a.75.75 0 011.5 0v.75a4.5 4.5 0 11-9 0v-.75a.75.75 0 011.5 0v.75z"
                        clip-rule="evenodd" />
                </svg>
            </div>
            <span><?php _e("Giỏ hàng", "mytheme") ?></span>
            <span class="count header-cart-count">
                (<?php echo WC()->cart->get_cart_contents_count(); ?>)
            </span>
        </a>
    <?php
        }
    }