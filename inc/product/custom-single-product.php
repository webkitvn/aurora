<?php

    // REMOVE DEFAULT TEMPLATE FUNCTIONS
    //remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
    //remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
    remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
    //remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );


    add_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 10);

    add_action( 'woocommerce_before_add_to_cart_button', function(){
        echo '<div class="add-to-cart-wrapper flex gap-6 items-stretch">';
    }, 5);

    add_action( 'woocommerce_after_add_to_cart_button', function(){
        echo '</div>';
    }, 15);

    add_action( 'woocommerce_before_add_to_cart_quantity', 'ecom_display_quantity_plus' );

    function ecom_display_quantity_plus() {
        $label = __("Số lượng", "mytheme");
        echo '<div class="quantity-wrapper"><span class="label hidden">'.$label.'</span><div class="quantity-buttons h-full inline-flex items-center border border-black rounded p-2"><button type="button" class="minus text-2xl" >-</button>';
    }
    
    add_action( 'woocommerce_after_add_to_cart_quantity', 'ecom_display_quantity_minus' );
    
    function ecom_display_quantity_minus() {
        echo '<button type="button" class="plus text-2xl" >+</button></div></div>';
    }

    add_action( 'wp_footer', 'ecom_add_cart_quantity_plus_minus' );
    
    function ecom_add_cart_quantity_plus_minus() {
        // Only run this on the single product page
        if ( ! is_product() ) return;
        ?>
        <script type="text/javascript">
        jQuery(document).ready(function($) {

            if ($('.quantity-wrapper .quantity').hasClass('hidden')) {
                $('.quantity-wrapper').addClass('d-none');
            }
            $('form.cart').on('click', 'button.plus, button.minus', function() {

                // Get current quantity values
                var qty = $(this).closest('form.cart').find('.qty');
                var val = parseFloat(qty.val());
                var max = parseFloat(qty.attr('max'));
                var min = parseFloat(qty.attr('min'));
                var step = parseFloat(qty.attr('step'));

                // Change the value if plus or minus
                if ($(this).is('.plus')) {
                    if (max && (max <= val)) {
                        qty.val(max);
                    } else {
                        qty.val(val + step);
                    }
                } else {
                    if (min && (min >= val)) {
                        qty.val(min);
                    } else if (val > 1) {
                        qty.val(val - step);
                    }
                }

            });

        });
        </script>
<?php
    }

// ADD "MUA NGAY" BEFORE ADD TO CART
function add_quick_buy_button(){
    echo '<button type="button" class="border border-orange-600 py-3 px-6 text-orange-600 font-bold rounded hover:bg-orange-600 hover:text-white buy_now_button">'.__('Mua ngay', 'mytheme').'</button>';
    echo '<input type="hidden" name="is_buy_now" class="is_buy_now" value="0" autocomplete="off"/>';
}

add_action( 'woocommerce_after_add_to_cart_quantity', 'add_quick_buy_button', 50);

add_filter('woocommerce_add_to_cart_redirect', 'redirect_to_checkout');
function redirect_to_checkout($redirect_url) {
    if (isset($_REQUEST['is_buy_now']) && $_REQUEST['is_buy_now']) {
        $redirect_url = wc_get_checkout_url(); //or wc_get_cart_url()
    }
    return $redirect_url;
}

// ADD CONTACT BUTTONS 
add_action('woocommerce_after_add_to_cart_button', 'add_product_contact_buttons', 30);

function add_product_contact_buttons(){
    wc_get_template_part('single-product/contact-buttons');
}


add_action('woocommerce_single_product_summary', function(){
    wc_get_template_part( 'single-product/extra-tabs');
}, 35);


//Related Products Only Same Category
function mytheme_filter_related_products_subcats_only($terms, $product_id) {
    // Check to see if this product has only one category ticked
	$prodterms = get_the_terms($product_id, 'product_cat');
	if (count($prodterms) === 1) {
		return $terms;
	}
    
    // Loop through the product categories and remove parent categories
	$subcategories = array();
	foreach ($prodterms as $k => $prodterm) {
		if ($prodterm->parent === 0) {
			unset($prodterms[$k]);
		} else {
			$subcategories[] = $prodterm->term_id;
		}
	}
	return $subcategories;
}
add_filter( 'woocommerce_get_related_product_cat_terms', 'mytheme_filter_related_products_subcats_only', 20, 2 );