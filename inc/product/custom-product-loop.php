<?php 
    remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
    remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );

    function custom_product_loop_title(){
        echo '<h3 class="woocommerce-loop-product__title line-clamp-1 font-semibold my-2 text-sm">' . get_the_title() .  '</h3>';
    }

    add_action( 'woocommerce_shop_loop_item_title', 'custom_product_loop_title', 10 );

    add_action ('woocommerce_after_shop_loop_item_title', function(){
        echo "<span class='viewmore text-orange-600 font-bold text-sm leading-[1] mt-4 inline-block'>".__("Xem chi tiết", "mytheme")."</span>";
    });