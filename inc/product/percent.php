<?php
    function sale_percentages() {
        global $product;
        if( $product->is_type('variable')){
            $percentages = array();
            
            $prices = $product->get_variation_prices();
            
            foreach( $prices['price'] as $key => $price ){
                
                if( $prices['regular_price'][$key] !== $price ){
                    
                    $percentages[] = round( 100 - ( floatval($prices['sale_price'][$key]) / floatval($prices['regular_price'][$key]) * 100 ) );
                }
            }
    
            if(empty($percentages)){
                return;
            }
            
            $percentage = '-' . max($percentages) . '%';
        } elseif( $product->is_type('grouped') ){
            $percentages = array();
            
            $children_ids = $product->get_children();
            
            foreach( $children_ids as $child_id ){
                $child_product = wc_get_product($child_id);
                $regular_price = (float) $child_product->get_regular_price();
                $sale_price    = (float) $child_product->get_sale_price();
                if ( $sale_price != 0 || ! empty($sale_price) ) {
                    
                    $percentages[] = round(100 - ($sale_price / $regular_price * 100));
                }
            }
            if(empty($percentages)){
                return;
            }
            $percentage = '-' . max($percentages) . '%';
        } else {
            $regular_price = (float) $product->get_regular_price();
            $sale_price    = (float) $product->get_sale_price();
            if ( $sale_price != 0 || ! empty($sale_price) ) {
                $percentage    = '-' . round(100 - ($sale_price / $regular_price * 100)) . '%';
            } else {
                return '';
            }
        }
        return $percentage;
    }