<?php
    add_filter( 'woocommerce_add_to_cart_fragments', 'mytheme_cart_count_fragments', 10, 1 );

    function mytheme_cart_count_fragments( $fragments ) {
        ob_start();
        $items_count = WC()->cart->get_cart_contents_count();
        ?>
        <span class="cart-count count header-cart-count">(<?php echo $items_count ? $items_count : 0; ?>)</span>
        <?php
            $fragments['.header-cart-count'] = ob_get_clean();
        return $fragments;
    }