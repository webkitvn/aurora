<?php
    function add_product_child_cats(){
        if(is_product_category()){
            $current = get_queried_object()->term_id;
            $childs = get_terms(array(
                'taxonomy' => 'product_cat',
                'parent' => $current,
                'child_of' => $current,
                'order_by' => 'menu_order'
            ));
            if(!empty($childs)) :
                echo "<ul class='woocommerce-child-cats'><li>" .__("View more:", "mytheme"). "</li>";
                foreach($childs as $child){
                    echo "<li><a href='" . get_term_link($child, 'product_cat') . "'>" . $child->name . "</a></li>";
                }
                echo "</ul>";
                endif;
        }
    }

    //add_action('woocommerce_archive_description', 'add_product_child_cats', 5);

    function add_product_category_extra_content(){
        if(is_product_category()){
            wc_get_template_part('content', 'category-extra-content');
        }
    }

    add_action('woocommerce_before_main_content', 'add_product_category_extra_content', 15);

    add_action('woocommerce_before_shop_loop', function(){
        wc_get_template_part('content', 'filter');
    }, 15);