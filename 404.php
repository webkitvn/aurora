<?php get_header() ?>
<div class="section page-404-content h-screen flex items-center justify-center">
    <div class="content max-w-5xl text-center">
        <h1 class="entry-title text-white text-3xl lg:text-5xl font-bold mb-10"><?php _e("Nội dung không tồn tại", "mytheme") ?></h1>
        <img class="max-w-[80%] lg:max-w-full w-[600px] h-auto mx-auto" src="<?php echo IMG ?>/404.svg" alt="404" width="700">
        <a class="custom-btn bg-white w-fit block mx-auto mt-10 rounded py-4 px-10 uppercase" href="<?php bloginfo( 'url' ) ?>"><?php _e("Về trang chủ", "mytheme") ?></a>
    </div>
</div>
<?php get_footer() ?>