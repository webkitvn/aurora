<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://kit.fontawesome.com/3ada7838f8.js" crossorigin="anonymous"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Lora:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&family=Work+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <?php wp_head(); ?>
    <?php the_field('header_scripts', 'option') ?>
</head>

<body <?php body_class(); ?>>
    <?php wp_body_open() ?>
    <header id="main-header" class="py-4 lg:pt-12 fixed w-full left-0 top-0 bg-transparent" data-headroom>
        <div class="wrapper flex items-center transition-all">
            <a href="<?php bloginfo('home') ?>"
                class="logo flex items-end flex-none transition-all duration-300">
                <?php 
                    $custom_logo_id = get_theme_mod( 'custom_logo' );
                    if ( has_custom_logo() ) {
                        echo wp_get_attachment_image($custom_logo_id, 'medium', false, array('alt'=>get_bloginfo('name')));
                    } else {
                        echo '<span class="site-name text-emerald-900">' . get_bloginfo('name') . '</span>';
                    }
                ?>
            </a>
            <nav class="nav flex flex-wrap justify-end flex-auto ml-4 gap-y-4">
                <div class="bottom-nav w-full gap-x-[2em] flex justify-end items-center">
                    <?php 
                        wp_nav_menu( array(
                            'theme_location' => 'primary',
                            'menu_class' => 'menu hidden lg:flex gap-x-[2em] font-semibold tracking-wide lg:!mr-6',
                            'container' => 'ul'
                        ) )
                    ?>
                    <button class="search-btn">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                            stroke="currentColor" class="w-6 h-6">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z" />
                        </svg>
                    </button>
                    <?php get_template_part('templates/socials') ?>
                    <button id="toggle-menu" class="toggle-menu-btn lg:!hidden" aria-label="MENU">
                        <span></span><span></span><span></span>
                    </button>
                </div>
            </nav>
        </div>
        <div class="fixed-search-box hidden fixed top-0 left-0 w-full text-black bg-white py-4 z-[200]">
            <button class="close absolute top-[50%] right-4 lg:right-8 -translate-y-[50%] text-gray-500"
                aria-label="Close">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                    stroke="currentColor" class="w-6 h-6">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
                </svg>
            </button>
            <div class="wrapper">
                <?php get_search_form() ?>
            </div>
        </div>
    </header>

    <div id="mobile-menu" class="fixed w-full left-0 bottom-0 rounded-t-3xl bg-emerald-900 p-8 pt-12 z-[9999] shadow-2xl shadow-emerald-600">
        <button class="close absolute top-2 right-2 text-white/50 text-2xl flex items-center justify-center w-10 h-10" aria-lable="close">
            <i class="ti ti-x"></i>
        </button>
        <div class="inner">
            <?php 
                wp_nav_menu( array(
                    'theme_location' => 'primary',
                    'menu_class' => 'menu font-semibold text-white tracking-wide text-center',
                    'container' => 'ul'
                ) )
            ?>
            <?php get_template_part('templates/socials') ?>
        </div>
    </div>