<?php gt_set_post_view(); ?>
<?php get_header() ?>
<section class="section single-content pt-16 lg:pt-32 pb-16">
    <div class="wrapper">
        <h1
            class="entry-title text-3xl lg:text-5xl lg:text-center text-emerald-800 font-lora font-extrabold !leading-snug max-w-5xl mx-auto">
            <?php the_title() ?></h1>
        <div class="meta mb-4 flex gap-8 items-center mt-10">
            <span class="time flex gap-1 items-center">
                <i class="ti ti-calendar-time"></i>
                <?php the_time('d/m/Y') ?></span>
            <span class="view flex gap-1 items-center">
                <i class="ti ti-eye"></i>
                <?php echo gt_get_post_view() ?>
            </span>
        </div>
        <div class="single-image aspect-video lg:aspect-[3/1] overflow-hidden mb-10 rounded-lg">
            <?php if(has_post_thumbnail()) : ?>
                <?php the_post_thumbnail('full', array('class'=>'w-full h-full object-cover object-center')) ?>
            <?php else : ?>
            <img class="w-full h-full object-cover object-center" src="https://source.unsplash.com/1600x800?natural"
                alt="<?php the_title() ?>">
            <?php endif; ?>
        </div>
        <div class="the-content lg:grid grid-cols-12 gap-10 relative">
            <div class="col-span-2">
                <div class="sticky lg:text-center" style="top:150px">
                    <span class="block mb-3 lg:text-center">SHARE</span>
                    <ul class="social-share icon-box flex gap-4 lg:block">
                        <li class="">
                            <a class="flex items-center justify-center w-10 h-10 border text-xl border-gray-300 rounded mx-auto hover:bg-emerald-800 hover:text-white hover:border-emerald-800 transition-colors duration-300 mb-3" href="#">
                                <i class="ti ti-brand-messenger"></i>
                            </a>
                        </li>
                        <li class="">
                            <a href="#" class="flex items-center justify-center w-10 h-10 border text-xl border-gray-300 rounded mx-auto hover:bg-emerald-800 hover:text-white hover:border-emerald-800 transition-colors duration-300 mb-3">
                                <i class="ti ti-brand-facebook"></i>
                            </a>
                        </li>
                        <li class="">
                            <a href="https://twitter.com/intent/tweet?text=<?php the_title() ?>&url=<?php the_permalink() ?>" class="flex items-center justify-center w-10 h-10 border text-xl border-gray-300 rounded mx-auto hover:bg-emerald-800 hover:text-white hover:border-emerald-800 transition-colors duration-300 mb-3" target="_parent">
                            <i class="ti ti-brand-twitter"></i>
                            </a>
                        </li>
                        <li class="">
                            <a href="#" class="flex items-center justify-center w-10 h-10 border text-xl border-gray-300 rounded mx-auto hover:bg-emerald-800 hover:text-white hover:border-emerald-800 transition-colors duration-300 mb-3">
                            <i class="ti ti-brand-linkedin"></i>
                            </a>
                        </li>
                        <li class="">
                            <a href="#" class="flex items-center justify-center w-10 h-10 border text-xl border-gray-300 rounded mx-auto hover:bg-emerald-800 hover:text-white hover:border-emerald-800 transition-colors duration-300 mb-3">
                            <i class="ti ti-brand-pinterest"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-span-8">
                <div class="entry-content">
                    <?php the_content() ?>
                </div>
                <div class="tags-wrapper border-t border-gray-200 pt-6 mt-6">
                    <?php the_tags( '', '', '' ) ?>
                </div>
                <div class="mt-10">
                    <span class="block mb-3">SHARE</span>
                    <ul class="social-share icon-box flex gap-4">
                        <li class="">
                            <a class="flex items-center justify-center w-10 h-10 border text-xl border-gray-300 rounded mx-auto hover:bg-emerald-800 hover:text-white hover:border-emerald-800 transition-colors duration-300 mb-3" href="#">
                                <i class="ti ti-brand-messenger"></i>
                            </a>
                        </li>
                        <li class="">
                            <a href="#" class="flex items-center justify-center w-10 h-10 border text-xl border-gray-300 rounded mx-auto hover:bg-emerald-800 hover:text-white hover:border-emerald-800 transition-colors duration-300 mb-3">
                                <i class="ti ti-brand-facebook"></i>
                            </a>
                        </li>
                        <li class="">
                            <a href="https://twitter.com/intent/tweet?text=<?php the_title() ?>&url=<?php the_permalink() ?>" class="flex items-center justify-center w-10 h-10 border text-xl border-gray-300 rounded mx-auto hover:bg-emerald-800 hover:text-white hover:border-emerald-800 transition-colors duration-300 mb-3" target="_parent">
                            <i class="ti ti-brand-twitter"></i>
                            </a>
                        </li>
                        <li class="">
                            <a href="#" class="flex items-center justify-center w-10 h-10 border text-xl border-gray-300 rounded mx-auto hover:bg-emerald-800 hover:text-white hover:border-emerald-800 transition-colors duration-300 mb-3">
                            <i class="ti ti-brand-linkedin"></i>
                            </a>
                        </li>
                        <li class="">
                            <a href="#" class="flex items-center justify-center w-10 h-10 border text-xl border-gray-300 rounded mx-auto hover:bg-emerald-800 hover:text-white hover:border-emerald-800 transition-colors duration-300 mb-3">
                            <i class="ti ti-brand-pinterest"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <?php get_template_part('templates/content', 'related') ?>
            </div>
        </div>
    </div>
</section>
<?php get_template_part('templates/newsletter') ?>
<?php get_footer() ?>