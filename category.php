<?php get_header() ?>
<?php get_template_part('templates/page-header') ?>
<section class="section lasted-post pb-24">
    <div class="wrapper">
        <?php echo do_shortcode('[ajax_load_more id="4580978520" loading_style="white" container_type="ul" post_type="post" posts_per_page="6" images_loaded="true" scroll="false" transition_container_classes="posts grid grid-cols-2 lg:grid-cols-3 gap-3 lg:gap-10" button_label="Xem thêm" button_loading_label="Loading" archive="true"]') ?>
    </div>
</section>
<?php get_template_part('templates/newsletter') ?>
<?php get_footer() ?>