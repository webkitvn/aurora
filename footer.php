    <footer id="footer" class="py-8 bg-white">
        <div class="wrapper">
            <div class="grid grid-cols-1 lg:grid-cols-2 justify-between items-center gap-8 lg:gap-20 text-emerald-950 text-sm">
                <div class="copyright">
                    <?php the_field('copyright', 'option') ?>
                </div>
                <?php 
                    $donate = get_field('donate', 'option');
                    if($donate['enable']) :
                        $img = $donate['button_img'];
                        $url = $donate['donate_url'];
                ?>
                <a class="donate w-40 lg:ml-auto block" href="<?php echo $url != '' ? $url : '#' ?>">
                    <?php if($img) {
                        echo wp_get_attachment_image($img['id'], 'medium', false, array());
                    } ?>
                </a>
                <?php endif; ?>
            </div>
        </div>
        </div>
    </footer>

    <?php wp_footer() ?>
    <?php the_field('footer_scripts', 'option') ?>
    </body>

    </html>