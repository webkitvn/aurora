<?php /* Template name: Contact */ ?>
<?php get_header() ?>
<div class="section section-contact py-20 lg:py-32">
    <div class="wrapper">
        <div class="inner lg:grid grid-cols-5 gap-10">
            <div class="col-span-3 px-[15px] lg:pl-0 lg:px-10">
                <h1 class="entry-title font-lora text-3xl lg:text-5xl mb-4 font-bold">
                    <?php _e("Để lại lời nhắn", "mytheme") ?>
                </h1>
                <div class="entry-content">
                    <?php the_content() ?>
                </div>
                <div class="form-wrapper mt-8">
                    <?php echo do_shortcode('[contact-form-7 id="5" title="Liên hệ"]') ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer() ?>