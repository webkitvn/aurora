module.exports = {
  content: ['**/**/*.{html,js,php}', './node_modules/tw-elements/dist/js/**/*.js'],
  plugins: [
    require('tw-elements/dist/plugin'),
    require('@tailwindcss/line-clamp'),
  ],
  theme: {
    extend: {
      fontFamily: {
        'lora': ['Lora', 'serif'],
        'worksan': ['Work Sans', 'san-serif'],
      }
    },
  },
}