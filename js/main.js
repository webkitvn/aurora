(function ($) {
	$(document).ready(function () {
		const headerHeight = $("#main-header").outerHeight();
		$("html").css("--header-height", headerHeight + "px");
	});

	$("#toggle-menu").on("click", function (e) {
		e.preventDefault();
		$("#mobile-menu, #toggle-menu").toggleClass("active");
	});

	$("#mobile-menu").on("click", ".close", function (e) {
		e.preventDefault();
		$("#mobile-menu, #toggle-menu").removeClass("active");
	});

	$(".fixed-search-box .close, #main-header .search-btn").on(
		"click",
		function (e) {
			e.preventDefault();
			$(".fixed-search-box").slideToggle(300);
		}
	);

	$(".entry-content.show-less").on("click", ".view__toggle", function (e) {
		e.preventDefault();
		$(".entry-content.show-less").toggleClass("active");
		$("html, body").animate(
			{
				scrollTop: $(".entry-content.show-less").offset().top - 150,
			},
			300
		);
	});
})(jQuery);


//Social share
// JavaScript code
document.addEventListener('DOMContentLoaded', function () {
	const shareIcons = document.querySelectorAll('.social-share a');

	shareIcons.forEach(function (icon) {
		icon.addEventListener('click', function (event) {
			event.preventDefault();
			const shareUrl = window.location.href; // Get the current page URL

			const socialMedia = this.querySelector('i').classList[1]; // Get the social media platform from the icon class

			// Implement your share functionality based on the social media platform
			switch (socialMedia) {
				case 'ti-brand-messenger':
					shareOnMessenger(shareUrl);
					break;
				case 'ti-brand-facebook':
					shareOnFacebook(shareUrl);
					break;
				case 'ti-brand-twitter':
					shareOnTwitter(shareUrl);
					break;
				case 'ti-brand-linkedin':
					shareOnLinkedIn(shareUrl);
					break;
				case 'ti-brand-pinterest':
					shareOnPinterest(shareUrl);
					break;
				default:
					// Handle unsupported platforms or fallback behavior
					console.log('Unsupported social media platform');
					break;
			}
		});
	});

	// Functions for sharing on different platforms
	function shareOnMessenger(url) {
		app_id = '1864542897125456';
		const shareWindowUrl = 'fb-messenger://share?link=' + encodeURIComponent(url) + '&app_id=' + encodeURIComponent(app_id);
		openShareWindow(shareWindowUrl);
	}

	function shareOnFacebook(url) {
		const shareWindowUrl = 'https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(url);
		openShareWindow(shareWindowUrl);
	}

	function shareOnTwitter(url) {
		const shareWindowUrl = 'https://twitter.com/share?url=' + encodeURIComponent(url);
		openShareWindow(shareWindowUrl);
	}

	function shareOnLinkedIn(url) {
		const shareWindowUrl = 'https://www.linkedin.com/shareArticle?url=' + encodeURIComponent(url);
		openShareWindow(shareWindowUrl);
	}

	function shareOnPinterest(url) {
		const shareWindowUrl = 'https://www.pinterest.com/pin/create/button/?url=' + encodeURIComponent(url);
		openShareWindow(shareWindowUrl);
	}

	function openShareWindow(url) {
		const width = 600;
		const height = 400;
		const left = window.innerWidth / 2 - width / 2;
		const top = window.innerHeight / 2 - height / 2;

		window.open(
			url,
			'Share',
			'width=' + width + ',height=' + height + ',top=' + top + ',left=' + left + ',toolbar=0,status=0'
		);
	}
});
