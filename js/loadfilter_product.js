(function ($) {
    $(document).ready(function(){
        var company =  $('#product-company-select').val();
        var url = '';
        $('#product-company-select').on('change', function(){
            company = $(this).val();
            $('.product-company-child-select').addClass('hidden');
            $('#select-' + company).removeClass('hidden').val('');
            if(company){
                url =  '/' + company;
                $('.filter-submit-btn').attr('href', url);
            }
        });

        $('.product-company-child-select').on('change', function(){
            child = $(this).val();
            if(child){
                url = '/' + company + '/' + child;
                $('.filter-submit-btn').attr('href', url);
            }
        });
    });
})(jQuery);