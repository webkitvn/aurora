(function ($) {
  const videos_swiper = new Swiper(".videos-slider .swiper", {
    // Optional parameters
    loop: true,
    slidesPerView: 3,
    spaceBetween: 5,
    observer: true,
    // If we need pagination
    pagination: {
      el: ".swiper-pagination",
    },

    // Navigation arrows
    navigation: {
      nextEl: ".videos-slider .swiper-button-next",
      prevEl: ".videos-slider .swiper-button-prev",
    },
  });

  const posts_swiper = new Swiper(".posts-slider .swiper", {
    // Optional parameters
    loop: true,
    slidesPerView: 3,
    spaceBetween: 5,
    observer: true,
    observeParents: true,
    // If we need pagination
    pagination: {
      el: ".posts-slider .swiper-pagination",
      clickable: true,
    },

    // Navigation arrows
    navigation: {
      nextEl: ".posts-slider .swiper-button-next",
      prevEl: ".posts-slider .swiper-button-prev",
    },
  });

  const review_swiper = new Swiper(".review-slider .swiper", {
    // Optional parameters
    loop: true,
    slidesPerView: 3,
    spaceBetween: 5,
    observer: true,
    observeParents: true,
    // If we need pagination
    pagination: {
      el: ".review-slider .swiper-pagination",
      clickable: true,
    },

    // Navigation arrows
    navigation: {
      nextEl: ".review-slider .swiper-button-next",
      prevEl: ".review-slider .swiper-button-prev",
    },
  });

  const tiktok_swiper = new Swiper(".tiktok-slider .swiper", {
    // Optional parameters
    loop: true,
    slidesPerView: 3,
    spaceBetween: 5,
    observer: true,
    observeParents: true,
    // If we need pagination
    pagination: {
      el: ".tiktok-slider .swiper-pagination",
      clickable: true,
    },

    // Navigation arrows
    navigation: {
      nextEl: ".tiktok-slider .swiper-button-next",
      prevEl: ".tiktok-slider .swiper-button-prev",
    },
  });

  const youtube_swiper = new Swiper(".youtube-slider .swiper", {
    // Optional parameters
    loop: true,
    slidesPerView: 3,
    spaceBetween: 5,
    observer: true,
    observeParents: true,
    // If we need pagination
    pagination: {
      el: ".youtube-slider .swiper-pagination",
      clickable: true,
    },

    // Navigation arrows
    navigation: {
      nextEl: ".youtube-slider .swiper-button-next",
      prevEl: ".youtube-slider .swiper-button-prev",
    },
  });

  const features_swiper = new Swiper(".features-slider .swiper", {
    // Optional parameters
    loop: false,
    slidesPerView: 3,
    spaceBetween: 20,
    // If we need pagination
    pagination: {
      el: ".swiper-pagination",
    },

    // Navigation arrows
    navigation: {
      nextEl: ".features-slider .swiper-button-next",
      prevEl: ".features-slider .swiper-button-prev",
    },
  });

  const featured_posts = new Swiper(".featured-posts .swiper", {
    // Optional parameters
    loop: true,
    slidesPerView: 4,
    spaceBetween: 15,
    // If we need pagination
    pagination: {
      el: ".swiper-pagination",
    },

    // Navigation arrows
    navigation: {
      nextEl: ".posts-slider .swiper-button-next",
      prevEl: ".posts-slider .swiper-button-prev",
    },
  });

  // $(".posts-slider .swiper").each(function (i, slider) {
  //   $ppLg = $(this).data("pp-lg") ? $(this).data("pp-lg") : 3;
  //   $ppMd = $(this).data("pp-md") ? $(this).data("pp-md") : 2;
  //   $ppSm = $(this).data("pp-sm") ? $(this).data("pp-sm") : 1;
  //   var imagesCarousel = new Swiper(slider, {
  //     direction: "horizontal",
  //     loop: true,
  //     slidesPerView: 3,
  //     spaceBetween: 5,
  //     speed: 300,
  //     observer: true,
  //     navigation: {
  //       nextEl: $(".posts-slider .swiper-button-next"),
  //       prevEl: $(".posts-slider .swiper-button-prev"),
  //     },
  //     pagination: {
  //       el: ".swiper-pagination",
  //       clickable: true,
  //     },
  //     breakpoints: {
  //       640: {
  //         slidesPerView: $ppSm,
  //       },
  //       768: {
  //         slidesPerView: $ppMd,
  //       },
  //       1024: {
  //         slidesPerView: $ppLg,
  //       },
  //     },
  //   });
  // });
})(jQuery);
