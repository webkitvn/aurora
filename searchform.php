<form action="<?php bloginfo('url') ?>" class="search-form relative rounded-[60px] border border-gray-300 w-[90%] lg:w-[700px] pl-5 pr-[50px] lg:mx-auto">
    <input class="text-lg h-[60px] w-full focus:outline-none outline-none rounded-[50px] bg-transparent" type="search" name="s" placeholder="<?php _e("Bạn muốn tìm kiếm gì ?", "mytheme") ?>">
    <button class="absolute w-[40px] h-[40px] top-[50%] right-[10px] -translate-y-[50%] flex items-center justify-center bg-transparent" type="submit" aria-label="Search">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
            class="w-6 h-6">
            <path stroke-linecap="round" stroke-linejoin="round"
                d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z" />
        </svg>
    </button>
</form>