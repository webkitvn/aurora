<?php global $post; ?>
<div class="post drop-shadow-sm rounded-lg overflow-hidden">
    <a href="<?php the_permalink() ?>" class="thumb block aspect-[5/3] overflow-hidden">
        <?php the_post_thumbnail( 'medium_large', array('class'=>'w-full h-full object-cover') ) ?>
    </a>
    <div class="text-box h-full bg-white p-3 lg:p-6">
        <h3 class="entry-title font-lora font-semibold mb-3 text-sm lg:text-xl line-clamp-2 lg:line-clamp-3">
            <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
        </h3>
        <div class="meta mb-4 lg:flex gap-8 items-center text-xs lg:text-base text-emerald-600">
            <span class="time flex gap-1 items-center">
                <i class="ti ti-calendar-time"></i>
                <?php echo get_time_ago(get_post_timestamp()) ?></span>
            <span class="view flex gap-1 items-center">
                <i class="ti ti-eye"></i>
                <?php echo gt_get_post_view() ?>
            </span>
        </div>
        <p class="exerpt line-clamp-3 leading-normal text-xs lg:text-base">
            <?php echo get_the_excerpt() ?>
        </p>
        <div class="tags tags-wrapper flex overflow-x-auto lg:overflow-x-visible lg:flex-wrap">
            <?php the_tags('', '', '') ?>
        </div>
    </div>
</div>