<?php /* Template name: About */ ?>
<?php get_header() ?>
<section class="section main-content py-32">
    <div class="wrapper">
        <div class="content grid grid-cols-1 lg:grid-cols-3 gap-16">
            <div class="image lg:col-span-1">
                <?php the_post_thumbnail('large', array('class'=>'w-full lg:w-auto max-w-full h-auto rounded-lg')) ?>
            </div>
            <div class="entry-content lg:col-span-2">
                <h1 class="entry-title text-3xl text-emerald-700"><?php the_title() ?></h1>
                <?php the_content() ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer() ?>