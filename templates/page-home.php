<?php /* Template name: Home */ ?>
<?php get_header() ?>
<?php get_template_part('templates/home/quote') ?>
<?php get_template_part('templates/home/posts') ?>
<?php get_template_part('templates/newsletter') ?>
<?php get_footer() ?>