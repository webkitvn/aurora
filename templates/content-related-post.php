<?php global $post; ?>
<div class="post grid grid-cols-4 gap-4 !drop-shadow-none !shadow-none">
    <a href="<?php the_permalink() ?>" class="thumb col-span-1 aspect-square rounded-md overflow-hidden">
        <?php the_post_thumbnail( 'medium', array('class'=>'w-full h-full object-cover') ) ?>
    </a>
    <div class="text-box col-span-3 flex flex-col justify-between">
        <h3 class="entry-title font-bold font-lora">
            <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
        </h3>
        <div class="meta mt-4 flex gap-8 items-center text-xs text-emerald-600">
            <span class="time flex gap-1 items-center">
                <i class="ti ti-calendar-time"></i>
                <?php echo get_time_ago(get_post_timestamp()) ?></span>
            <span class="view flex gap-1 items-center">
                <i class="ti ti-eye"></i>
                <?php echo gt_get_post_view() ?>
            </span>
        </div>
    </div>
</div>