<section class="section newsletter py-32">
    <div class="wrapper">
        <h2 class="text-center text-3xl font-lora font-bold max-w-md mb-6 mx-auto">Đăng ký để cập nhật bài viết mới nhất</h2>
        <?php echo do_shortcode('[contact-form-7 id="118" title="Newsletter"]') ?>
    </div>
</section>