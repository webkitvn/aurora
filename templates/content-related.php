<?php 
    global $post;
    if ( function_exists( 'get_crp_posts_id' ) ) :
        $relatedPosts = get_crp_posts_id( array(
            'limit' => 4,
        ));
        $relatedPosts = wp_list_pluck( (array) $relatedPosts, 'ID' );
        $query = new WP_Query(array(
            'post_type' => 'post',
            'post__in' => $relatedPosts,
            'posts_per_page' => 4,
            'no_found_rows' => 1
        ));
        if($query->have_posts()) :
?>
<div class="related-posts mt-10">
    <h3 class="related-post-title text-emerald-700 mb-6 text-2xl font-bold font-lora">Xem thêm</h3>

    <div class="posts grid grid-cols-1 lg:grid-cols-2 gap-8">
        <?php while($query->have_posts()) : $query->the_post(); ?>
            <?php get_template_part('templates/content', 'related-post') ?>
        <?php endwhile; wp_reset_query(); ?>
    </div>
</div>
<?php endif; endif; ?>