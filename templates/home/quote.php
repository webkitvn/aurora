<section class="section home-quotes py-[20vh] lg:py-[200px] relative">
    <div class="bg absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 z-0">
        <?php get_template_part('templates/wave') ?>
    </div>
    <div class="wrapper relative z-10">
        <div class="content text-center">
            <?php 
                $quote_img = get_field('quote_avatar', 'option');
                if($quote_img) :
            ?>
            <div class="avatar aspect-square w-32 h-32 overflow-hidden rounded-full mx-auto mb-8">
                <?php echo wp_get_attachment_image($quote_img['id'], 'medium_large', false, array('class'=>'w-full h-full object-cover')) ?>
            </div>
            <?php endif; ?>
            <div class="text-box">
                <?php if(get_field('quote_text', 'option')) : ?>
                <h1 class="text-2xl lg:text-5xl font-bold font-lora text-emerald-800 leading-tight">
                    <?php the_field('quote_text', 'option') ?>
                </h1>
                <?php endif; ?>
                <?php if(get_field('quote_author', 'option')) : ?>
                    <p class="author text-lg lg:text-2xl mt-5">--<span><?php the_field('quote_author', 'option') ?></span></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>