<?php 
    $query = new WP_Query(array(
        'posts_per_page' => 6,
        'post_type' => 'post'
    ));
    if($query->have_posts()) : 
?>
<section class="section lasted-post pb-24">
    <div class="wrapper">
        <div class="posts grid grid-cols-2 lg:grid-cols-3 gap-3 lg:gap-10">
            <?php 
                while($query->have_posts()){
                    $query->the_post();
                    get_template_part('templates/content', 'post');
                }
            ?>
        </div>
        <a href="<?php echo get_category_link(1) ?>"
            class="custom-btn w-fit flex item-center gap-4 text-lg font-semibold text-emerald-100 bg-emerald-800 rounded py-4 px-12 mx-auto mt-12 hover:bg-emerald-900 transition-colors duration-300">Xem
            thêm bài khác</a>
    </div>
</section>
<?php endif; ?>