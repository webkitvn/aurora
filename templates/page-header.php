<section class="page-header pt-16 lg:pt-32 pb-20">
    <div class="wrapper text-center">
        <h1 class="entry-title font-lora font-extrabold text-3xl lg:text-5xl text-emerald-900 mb-4">
            <?php 
                if(is_singular()){
                    the_title();
                } 
                elseif(is_category()){
                    single_cat_title();
                }
                elseif(is_tag()){
                    echo '#';
                    single_cat_title();
                }
                elseif(is_search()){
                    echo __("Kết quả tìm kiếm:", "mytheme") . " " . get_search_query();
                }
            ?>
        </h1>
        <?php if (function_exists('rank_math_the_breadcrumbs')) : ?>
        <div id="breadcrumbs" class="text-sm tracking-wide">
            <?php rank_math_the_breadcrumbs(); ?>
        </div>
        <?php endif; ?>
    </div>
</section>