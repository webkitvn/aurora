<?php
define('THEME_PATH',dirname(__FILE__));
define('THEME_URL',get_bloginfo('stylesheet_directory'));
define('IMG',get_bloginfo('stylesheet_directory')."/img");
define('HOME',home_url());
if ( ! function_exists( 'mytheme_setup' ) ) :
	function mytheme_setup() {
		load_theme_textdomain( 'mytheme', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary Menu', 'mytheme' ),
			'sub-menu' => esc_html__( 'Sub Menu', 'mytheme' ),
			'news-menu' => esc_html__( 'News Menu', 'mytheme' ),
			'mobile-menu' => esc_html__( 'Mobile Menu', 'mytheme' ),
			'footer_1' => esc_html__( 'Footer 1', 'mytheme' ),
			'footer_2' => esc_html__( 'Footer 2', 'mytheme' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );
		add_theme_support( 'custom-logo' );
	}
endif;
add_action( 'after_setup_theme', 'mytheme_setup' );

/**
 * Enqueue scripts and styles.
 */
function mytheme_scripts() {
	$ver = '1.0';
	//wp_enqueue_style('swipercss', 'https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css', array(), $ver, 'all');
	//wp_enqueue_style('fancybox', 'https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css', array(), $ver, 'all');
	wp_enqueue_style('tabler', 'https://cdn.jsdelivr.net/npm/@tabler/icons@latest/iconfont/tabler-icons.min.css', array(), $ver, 'all');
	wp_enqueue_style( 'mytheme-style', get_stylesheet_uri() );
	wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css', array(), $ver, 'all' );
	wp_enqueue_style( 'custom', get_template_directory_uri() . '/css/custom.css', array(), $ver, 'all' );

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script('headroom', get_template_directory_uri().'/js/headroom.min.js', array(), $ver, true);
	wp_enqueue_script('tablerjs', 'https://cdn.jsdelivr.net/npm/@tabler/icons@latest/icons-react/dist/index.umd.min.js', array(), $ver, true);
	//wp_enqueue_script('swiperjs', 'https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js', array(), $ver, true);
	//wp_enqueue_script('fancybox', 'https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js', array(), $ver, true);
	//wp_enqueue_script('matchHeight', get_template_directory_uri().'/js/matchHeight.min.js', array(), $ver, true);
	//wp_enqueue_script('tailwind-elements', get_template_directory_uri().'/node_modules/tw-elements/dist/js/index.min.js', array(), $ver, true);
	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array(), $ver, true );
}
add_action( 'wp_enqueue_scripts', 'mytheme_scripts' );

if ( function_exists( 'add_theme_support' ) ) { 
	add_theme_support( 'post-thumbnails', array('post', 'us_service'));
	set_post_thumbnail_size(600, 400, true ); // default Post Thumbnail dimensions (cropped)
}
if ( function_exists( 'add_image_size' ) ) {
	//add_image_size('vertical-size', 400, 600, true );
}
function my_admin_theme_style() {
    wp_enqueue_style('my-admin-style', get_template_directory_uri() . '/css/customadmin.css');
}
add_action('admin_enqueue_scripts', 'my_admin_theme_style');

if( function_exists('acf_add_options_page') ) {
	require get_template_directory() . '/inc/options.php';
}
require get_template_directory() . '/inc/cleanup.php';
require get_template_directory() . '/inc/viewcount.php';


function wpdocs_custom_excerpt_length( $length ) {
    return 25;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );
function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );
add_filter('wpcf7_autop_or_not', '__return_false');
function get_time_ago( $time )
{
    $time_difference = time() - $time;

    if( $time_difference < 1 ) { return 'mới'; }
    $condition = array( 12 * 30 * 24 * 60 * 60 =>  'năm',
                30 * 24 * 60 * 60       =>  'tháng',
                24 * 60 * 60            =>  'ngày',
                60 * 60                 =>  'giờ',
                60                      =>  'phút',
                1                       =>  'giây'
    );

    foreach( $condition as $secs => $str )
    {
        $d = $time_difference / $secs;

        if( $d >= 1 )
        {
            $t = round( $d );
            return $t . ' ' . $str . ' trước';
        }
    }
}

//EMBED YOUTUBE
function youtube_shortcode($atts) {
    $a = shortcode_atts( array(
        'id' => ''
    ), $atts );

    $video_id = $a['id'];

    $output = '<div class="aspect-w-16 aspect-h-9 my-4">';
    $output .= '<iframe src="https://www.youtube.com/embed/' . $video_id . '" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
    $output .= '</div>';

    return $output;
}
add_shortcode('youtube', 'youtube_shortcode');